program AoC2020Day1;

uses sysutils;

var
	data: array[0..199] of Integer;
	g   : array[0..3] of Char = (' ',' ',' ',' ');
	C   : Char;
    f   : File of char;
	h,k,l: Integer;
	a,b,d: LongInt;

procedure parseToInt();
var 
	sum: LongInt;
	i  : Integer;
	m  : Integer;

begin
	sum := 0;
	m   := 1;
	for i:=3 downto 0 do 
		Begin
		if (g[i]=' ') then continue;
		sum := sum + m*(ord(g[i])-48);	
		m := m*10;
		end;
	data[k] := sum;
end;

begin
  l := 0;
  h := 0;
  k := 0;
  C := 'l';
  Assign(f,'sample.txt');
  Reset(f);
  While not Eof(f) and (C<>' ') do
    Begin
    Read (f,C);
	if (C=chr(10)) then 
		begin
			h := 0;
			parseToInt;
			(* Parse g to int and assign to data[k] *)
			k := k+1;
			g[0] := ' ';
			g[1] := ' ';
			g[2] := ' ';
			g[3] := ' ';
		end
	else
		begin
			g[h] := C;
			h := h + 1;
    	end;
	end;
  Close (f);
  {Print data}
  {for l:=0 to 199 do writeln(data[l]);}

	for a:=0 to 197 do
	begin
		for b:=a+1 to 198 do
		begin
			for d:=b+1 to 199 do
			begin
				if ((data[a]+data[b]+data[d])=2020) then
					writeln('Resultado: ',data[a]*data[b]*data[d]);
			end;
		end;
	end;


end.
