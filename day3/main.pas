program AoC2020Day3;

uses sysutils;

var 
	c : Char;
	f : file of Char;
	data: Array[0..322,0..30] of Char;
	x,y : Integer;
	position : Integer;
	trees : Integer;

procedure readNChars(num:Integer);
var i: Integer;
begin
	for i:=1 to num do
	begin
		if not eof(f) then
			read(f,c);
	end;
end;

procedure steps(yStep:Integer;xStep:Integer);
begin
	trees := 0;
	x := 0;
	y := 0;
	while x<323 do
	Begin
		if y>30 then y := y-31;
		if (data[x,y]='#') then trees := trees+1;
		x := x+xStep;
		y := y+yStep;
	End;
	Writeln('Right ',yStep,'-Down ',xStep);
	writeln('Trees: ',trees);
	writeln('#################');
end;

begin
	trees := 0;
	c := 'h';
	assign(f,'sample.txt');
	reset(f);
	(*if not eof(f) then read(f,c);*)
	for x:=0 to 322 do
	begin
		for y:=0 to 30 do
		begin
			if not eof(f) then read(f,c);
			if (ord(c)=10) then read(f,c);
			data[x,y] := c;
		end;
	end;
	position := 0;
(*
	x := 0;
	y := 0;
	xStep := 1;
	yStep := 3;
	while x<323 do
	Begin
		if y>30 then y := y-31;
		if (data[x,y]='#') then trees := trees+1;
		x := x+xStep;
		y := y+yStep;
	End;
	writeln('Trees: ',trees);
*)
	steps(3,1);
	steps(1,1);
	steps(5,1);
	steps(7,1);
	steps(1,2);
	close(f);
end.
