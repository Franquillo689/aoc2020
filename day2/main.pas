program AoC2020Day2;

uses sysutils;

var
	{File reading variables nedeed}
	c : Char;
	f : File of Char;
	{}
	ordC : Integer;
	passwAllowCount,passAllowed : LongInt;
	min,max : Integer;
	minAndMaxChars : Array[0..1, 0..1] of Char;
	charPass : Char; {This is the char inside password we are counting}
	position: Integer;
	posMin,posMax: Boolean;

procedure assignToMin();

begin
	if minAndMaxChars[0,0]=' ' then
		minAndMaxChars[0,0] := c
	else
		minAndMaxChars[0,1] := c;
end;

function charToInt(num:Char): Integer;

begin
	if (ord(num)-48)<0 then
		charToInt := 0
	else 
		charToInt := ord(num)-48;
end;

procedure setMinAndMax();

begin
	if minAndMaxChars[0,1]=' ' then
		min := charToInt(minAndMaxChars[0,0])
	else
		min := charToInt(minAndMaxChars[0,1]) + 10*charToInt(minAndMaxChars[0,0]);

	if minAndMaxChars[1,1]=' ' then
		max := charToInt(minAndMaxChars[1,0])
	else
		max := charToInt(minAndMaxChars[1,1]) + 10*charToInt(minAndMaxChars[1,0]);
end;

procedure resetMinAndMax();
begin
	minAndMaxChars[0,0] := ' ';
	minAndMaxChars[0,1] := ' ';
	minAndMaxChars[1,0] := ' ';
	minAndMaxChars[1,1] := ' ';
	min := -1;
	max := -1;
	position := 1;
	posMin := false;
	posMax := false;
end;

begin
	resetMinAndMax;
	{ Initialize to '1' to say that charPass is empty }
	charPass := '1';
	passwAllowCount := 0;
	passAllowed := 0;
	c := 'l';{Any character}
	assign(f,'sample.txt');
	reset(f);
	while not eof(f) do
	begin
		read(f,c);
		ordC := ord(c);
		{ If c is \n then reset values }
		if (ordC=10) then
		begin
			{DebugPart}
			writeln('Min and max: ',min,'-',max);
			writeln('Letter:',charPass);
			writeln('Count: ',passwAllowCount);
			if posMin xor posMax then
				passAllowed := passAllowed+1;
			resetMinAndMax;
			charPass := '1';
			passwAllowCount := 0;
			continue;
		end;

		if (ordC>47) and (ordC<58) then
			assignToMin;
			
		if c='-' then
		begin
			read(f,c);
			minAndMaxChars[1,0] := c;
			read(f,c);
			ordC := ord(c);
			minAndMaxChars[1,1] := c;
			setMinAndMax;
		end;

		if (ordC>96) and (ordC<123) and (charPass='1') then
			begin charPass := c;continue;end;
		if (ordC>96) and (ordC<123) and (charPass<>'1') then
			begin
				if (c=charPass) then
				begin
					if position=min then posMin := true;
					if position=max then posMax := true;
				end;
			position := position+1;	
			end;
	end;	
	close(f);
	writeln('Password allowed: ',passAllowed);
end.
