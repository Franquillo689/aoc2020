program AoCDay4;

uses sysutils;

{ Structure for passports }
type
passport = record
	byr: Integer;
	iyr: Integer;
	eyr: Integer;
	hgt: Integer;
	hcl: LongInt;
	ecl: LongInt;
	pid: LongInt;
	cid: Integer;
end;

var
	c : char;
	f : file of char;
	

procedure goToLine(num:LongInt);
{ Goes to line num on sample.txt }
begin
	if (num<0) then exit;
	Reset(f);
	if not eof(f) then Read(f,c);
	while not eof(f) and (num>1) do
	begin
		if (ord(c)=10) then
			num := num-1;
		Read(f,c);
	end;
	if eof(f) then 
	begin
		Reset(f);
		Read(f,c);
	end;
end;

procedure readLine(num:LongInt);
(*
	Print num line of sample.txt 
	and left cursor on next line.
*)
begin
	goToLine(num);
	while not eof(f) and (ord(c)<>10) do
	begin
		write(c);
		Read(f,c);
	end;
	if not eof(f) then Read(f,c)
	else Reset(f);
end;


begin
	c := 'k';
	Assign(f,'sample.txt');
	Reset(f);

	if not eof(f) then
		readLine(959);

	Close(f);
end.
